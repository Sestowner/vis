-- complete word at selection location using vis-complete(1)

vis:map(vis.modes.INSERT, "<C-n>", function()
	local win = vis.win
	local file = win.file
	local pos = win.selection.pos
	if not pos then return end
	local range = file:text_object_word(pos > 0 and pos-1 or pos);
	if not range then return end
	if range.finish > pos then range.finish = pos end
	if range.start == range.finish then return end
	local prefix = file:content(range)
	if not prefix then return end

	vis:feedkeys("<vis-selections-save><vis-mode-normal-escape><vis-mode-normal-escape>")
	-- collect words starting with prefix
	vis:command("x/\\b" .. prefix .. "\\w+/")
	local candidates = {}
	for sel in win:selections_iterator() do
		table.insert(candidates, file:content(sel.range))
	end
	vis:feedkeys("<vis-mode-normal-escape><vis-mode-normal-escape><vis-selections-restore>")

	vis.mode = vis.modes.INSERT

	if #candidates == 1 then return end

	candidates = table.concat(candidates, "\n")

	local status, out, err = vis:pipe("printf -- '" .. candidates .. "' | sort -u | fzy", false)
	if status ~= 0 or not out then
		if err then vis:info(err) end
		return
	end

	out = out:sub(#prefix + 1, #out - 1)

	for selection in win:selections_iterator() do
		pos = selection.pos
		file:insert(pos, out)
		selection.pos = pos + #out
	end
end, "Complete word in file")
